def deadline_score(pass_date: str, deadline_date: str) -> int:
    """
    Рассчёт полученной оценки на основании дат сдачи и дедлайна

    На вход функции подаётся две строки – дата сдачи работы и
    дата дедлайна работы. Функция определяет, исходя из даты дедлайна и
    фактической сдачи, сколько баллов получит студент за сданную работу

    :param pass_date: Дата сдачи работы в формате ДД.ММ.ГГГГ
    :type pass_date: str
    :param deadline_date: Дата дедлайна работы в формате ДД.ММ.ГГГГ
    :type deadline_date: str
    :return: grade, рассчитанная оценка
    :rtype: int

    >>> deadline_score('01.09.2020', '02.09.2020')
    5
    >>> deadline_score('03.09.2020', '02.09.2020')
    4
    >>> deadline_score('31.12.2020', '02.09.2020')
    0
    """
    from datetime import datetime
    if pass_date == deadline_date:
        return 5
    pass_date = datetime.strptime(pass_date, '%d.%m.%Y')
    deadline_date = datetime.strptime(deadline_date, '%d.%m.%Y')
    diff_date = str(pass_date - deadline_date)
    diff_date = int(diff_date[:diff_date.index(' ')])
    grade = 5
    while grade != 0 and diff_date > 0:
        diff_date -= 7
        grade -= 1
    return grade
