"""
Модуль для тестирования основных функций с помощью библиотеки pytest

test_deadline_score - тестирование функции deadline_score
test_late_list - тестирование функции late_list
"""

import pytest
from ..grade_calc import deadline_score
from ..late_list_getter import late_list

passes = [
    ('11.11.2021', '11.11.2021', 5),
    ('15.11.2021', '11.11.2021', 4),
    ('11.11.2021', '15.11.2020', 0)
]

passes_list = [
    ({'Иванов': '11.11.2021', 'Петров': '21.11.2021', 'X Æ A-12': '7.09.2021'}, '11.11.2021', ['Петров']),
    ({'Martyshov': '07.01.2022', 'Mageroff': '08.01.2022', 'Mochalov': '07.01.2022'}, '30.12.2021', ['Mageroff', 'Martyshov', 'Mochalov'])
]


@pytest.mark.parametrize("pass_date, deadline_date, expected_result", passes)
def test_deadline_score(pass_date, deadline_date, expected_result):
    """
    Проверка на корректность работы функции deadline_score, на расчёт полученной оценки
    """
    assert deadline_score(pass_date, deadline_date) == expected_result


@pytest.fixture
def data_unpacker(request):
    return request.param


@pytest.mark.parametrize("data_unpacker", passes_list, indirect=True)
def test_late_list(data_unpacker):
    """
    Проверка на корректность работы функции late_list, на поиск людей, сдавших после дедлайна
    """
    result = late_list(*data_unpacker[:2])
    expected_result = data_unpacker[-1]
    assert result == expected_result
