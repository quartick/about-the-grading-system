"""
Модуль для тестирования основных функций с помощью модуля unittest

test_deadline_score - тестирование функции deadline_score
test_late_list - тестирование функции late_list
"""

import unittest
from ..grade_calc import deadline_score
from ..late_list_getter import late_list


passes = [
    ('11.11.2021', '11.11.2021', 5),
    ('15.11.2021', '11.11.2021', 4),
    ('11.11.2021', '15.11.2020', 0)
]

passes_list = [
    ({'Иванов': '11.11.2021', 'Петров': '21.11.2021', 'X Æ A-12': '7.09.2021'}, '11.11.2021', ['Петров']),
    ({'Martyshov': '07.01.2022', 'Mageroff': '08.01.2022', 'Mochalov': '07.01.2022'}, '30.12.2021', ['Mageroff', 'Martyshov', 'Mochalov'])
]


class GetGrade(unittest.TestCase):
    def test_deadline_score(self):
        for pdate, ddate, expected in passes:
            with self.subTest(pdate=pdate, ddate=ddate, expected=expected):
                result = deadline_score(pdate, ddate)
                self.assertEqual(result, expected)


class GetLateList(unittest.TestCase):
    def test_late_list(self):
        for grades, ddate, expected in passes_list:
            with self.subTest(grades=grades, ddate=ddate, expected=expected):
                result = late_list(grades, ddate)
                self.assertEqual(result, expected)
