# Сторонние импорты
import click
import pytest

# Локальные импорты
from grade_calc import deadline_score
from late_list_getter import late_list
from input_getter import get_input


@click.command()
@click.option("--mode", "-m",
              type=click.Choice(["grade", "list", "tests"], case_sensitive=True),
              help="Выберите режим работы: grade - рассчитать оценку; "
                   "list - найти опоздавших; tests - вывод тестов.")
def main(mode: str):
    """
    Интерфейс командной строки
    """
    if mode == 'grade':
        raw_input = get_input()
        print(f'Ваша оценка - {deadline_score(*raw_input)}!')
    elif mode == 'list':
        raw_input = get_input()
        raw_dict = {}
        for i in range(0, len(raw_input) - 1, 2):
            raw_dict[raw_input[i]] = raw_input[i + 1]
        print(*late_list(grades=raw_dict, deadline_date=raw_input[-1]))
    elif mode == 'tests':
        pytest.main(['-v'])


if __name__ == '__main__':
    main()
