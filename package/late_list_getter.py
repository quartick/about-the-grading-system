def late_list(grades: dict, deadline_date: str) -> list[str]:
    """
    Поиск студентов, сдавших после дедлайна

    На вход функции подаётся словарь (ключ – фамилия
    студента, значение – дата сдачи) и дата дедлайна работы.
    Функция определяет, сколько студентов из определённого списка
    сдали работу позже намеченного срока, и возвращает список фамилий этих
    студентов в алфавитном порядке.

    :param grades: словарь, где ключ – фамилия студента, значение – дата сдачи
    :type grades: dict
    :param deadline_date: Дата дедлайна работы формата ДД.ММ.ГГГГ
    :type deadline_date: str
    :return: name_list, список студентов, сдавших после дедлайна
    :rtype: List[str]

    >>> late_list({'Иванов': '03.09.2020', 'Петров': '01.09.2020'}, '02.09.2020')
    ['Иванов']
    """
    from datetime import datetime
    name_list = []
    deadline_date = datetime.strptime(deadline_date, '%d.%m.%Y')
    for name, pass_date in grades.items():
        pass_date = datetime.strptime(pass_date, '%d.%m.%Y')
        if pass_date > deadline_date:
            name_list.append(name)
    return sorted(name_list)
