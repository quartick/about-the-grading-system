«Про систему оценивания»
===

### Пример из жизни

У многих лабораторных и практических работ есть определённая дата
сдачи – дедлайн. Многие преподаватели продолжают принимать работы и после
дедлайна, однако в таком случае баллы за выполнение уменьшаются. Обычно
правило для уменьшения баллов одно – за каждую неделю задержки итоговая
оценка уменьшается на один балл (пропустил 1 неделю – снят 1 балл, за 5 недель
– все 5 баллов). Оценка за работу варьируется от 5 до 0 баллов, отрицательной
она быть не может (пропустил три месяца – всё равно снимаем 5 баллов).
Также важной информацией может стать статистика того, сколько всего
студентов сдали работу позже назначенного срока.
![](https://papik.pro/uploads/posts/2021-09/1631268885_1-papik-pro-p-student-karikatura-1.png)

Для ознакомления с основынми терминами и системой оценивания ([Памятка студенту СФУ](https://about.sfu-kras.ru/docs/8438/pdf/231087))
### Программная реализация
##### Рассчёт полученной оценки
Напишем функцию, которая будет вычислять оценку студента по двум датам: дате сдачи работы, и дате дедлайна.

```python
def deadline_score(pass_date: str, deadline_date: str) -> int:
    from datetime import datetime
    if pass_date == deadline_date:
        return 5
    pass_date = datetime.strptime(pass_date, '%d.%m.%Y')
    deadline_date = datetime.strptime(deadline_date, '%d.%m.%Y')
    diff_date = str(pass_date - deadline_date)
    diff_date = int(diff_date[:diff_date.index(' ')])
    grade = 5
    while grade != 0 and diff_date > 0:
        diff_date -= 7
        grade -= 1
    return grade
```

##### Поиск студентов, сдавших после дедлайна
Усовершенствуем программу, добавив в неё функцию для поиска тех студентов, кто сдал позже назначенного дедлайна, на основе функции описанной выше.

```python
def late_list(grades: dict, deadline_date: str) -> list[str]:
    from datetime import datetime
    name_list = []
    deadline_date = datetime.strptime(deadline_date, '%d.%m.%Y')
    for name, pass_date in grades.items():
        pass_date = datetime.strptime(pass_date, '%d.%m.%Y')
        if pass_date > deadline_date:
            name_list.append(name)
    return sorted(name_list)
```

На данном этапе программа полностью завершена и готова к эксплуатации.
